package Sample2;

public class Main {
	public static void main(String[] args){
		Pickup_car pickup = new Pickup_car("2500 cc");
		Saloon_car saloon = new Saloon_car("2000 cc");
		
		HighlyPopular_Nissan highlypopular = new HighlyPopular_Nissan();
		System.out.println(highlypopular.type_car_nissan(pickup));
		System.out.println(highlypopular.type_car_nissan(saloon));
		System.out.println(highlypopular.type_car_nissan((Car_of_Nissan) saloon));
	}

}